import pygame
from Game.settings import *


class Menu:
    def __init__(self, game):
        self.Game = game
        self.menu_width, self.menu_height = WIDTH/2, HEIGHT/2
        self.run_display = True
        self.cursor_rect = pygame.Rect(0, 0, 20, 20)
        self.offset = -100

    def draw_cursor(self):
        self.Game.draw_text('>', 20, self.cursor_rect.x, self.cursor_rect.y - 5)

    def blit_screen(self):
        self.Game.window.blit(self.Game.display, (0, 0))
        pygame.display.update()
        self.Game.reset_keys()


class MainMenu(Menu):
    def __init__(self, Game):
        Menu.__init__(self, Game)
        self.cursor_state = "Start"
        self.startX, self.startY = self.menu_width, self.menu_height + 30
        self.optionsX, self.optionsY = self.menu_width, self.menu_height + 80
        self.cursor_rect.midtop = (self.startX + self.offset, self.startY)

    def display_menu(self):
        self.run_display = True
        while self.run_display:
            self.Game.check_events()
            self.check_input()
            self.Game.display.fill(BLACK)
            self.Game.draw_text("Gra Survival", 60, WIDTH/2, 200)
            self.Game.draw_text("Start Game", 30, self.startX, self.startY)
            self.Game.draw_text("Options", 30, self.optionsX, self.optionsY)
            self.Game.draw_text("Exit", 30, self.optionsX, self.optionsY + 50)
            self.draw_cursor()
            self.blit_screen()

    def move_cursor(self):
        if self.Game.DOWN_KEY:
            if self.cursor_state == "Start":
                self.move_function("Options", self.optionsX, self.optionsY)
            elif self.cursor_state == "Options":
                self.move_function("Exit", self.optionsX, self.optionsY + 50)
            else:
                self.move_function("Start", self.startX, self.startY)
        if self.Game.UP_KEY:
            if self.cursor_state == "Start":
                self.move_function("Exit", self.optionsX, self.optionsY + 50)
            elif self.cursor_state == "Options":
                self.move_function("Start", self.startX, self.startY)
            else:
                self.move_function("Options", self.optionsX, self.optionsY)


    def move_function(self, state, x, y):
        self.cursor_rect.midtop = (x + self.offset, y)
        self.cursor_state = state


    def check_input(self):
        self.move_cursor()
        if self.Game.START_KEY:
            if self.cursor_state == "Start":
                self.Game.playing = True
            elif self.cursor_state == "Options":
                self.Game.current_menu = self.Game.options_menu
            elif self.cursor_state == "Exit":
                quit()
            self.run_display = False


class OptionsMenu(Menu):
    def __init__(self, Game):
        Menu.__init__(self, Game)
        self.cursor_state = "Volume"
        self.volumeX, self.volumeY = self.menu_width, self.menu_height + 30
        self.controlsX, self.controlsY = self.menu_width, self.menu_height + 80
        self.cursor_rect.midtop = (self.volumeX + self.offset, self.volumeY)

    def display_menu(self):
        self.run_display = True
        while self.run_display:
            self.Game.check_events()
            self.check_inputs()
            self.Game.display.fill(BLACK)
            self.Game.draw_text("Options", 60, WIDTH / 2, 200)
            self.Game.draw_text("Volume", 30, self.volumeX, self.volumeY)
            self.Game.draw_text("Controls", 30, self.controlsX, self.controlsY)
            self.draw_cursor()
            self.blit_screen()

    def check_inputs(self):
        if self.Game.BACK_KEY:
            self.Game.current_menu = self.Game.main_menu
        self.run_display = False
        self.move_cursor()

    def move_cursor(self):
        if self.Game.DOWN_KEY or self.Game.UP_KEY:
            if self.cursor_state == "Volume":
                self.cursor_rect.midtop = (self.controlsX + self.offset, self.controlsY)
                self.cursor_state = "Controls"
            elif self.cursor_state == "Controls":
                self.cursor_rect.midtop = (self.volumeX + self.offset, self.volumeY)
                self.cursor_state = "Volume"
        if self.Game.START_KEY:
            if self.cursor_state == "Volume":
                self.Game.current_menu = self.Game.volume_menu
            elif self.cursor_state == "Controls":
                self.Game.current_menu = self.Game.controls_menu


class VolumeMenu(Menu):
    def __init__(self, Game):
        Menu.__init__(self, Game)
        self.cursor_state = "Sound"
        self.soundX, self.soundY = self.menu_width - 50, self.menu_height + 30
        self.musicX, self.musicY = self.menu_width - 50, self.menu_height + 100
        self.cursor_rect.midtop = (self.soundX + self.offset, self.soundY)
        self.volume_dict = {"Sound" : 0, "Music" : 0}
        with open("Txt_files\\volume.txt", "r") as file:
            data = file.readlines()
            self.volume_dict.update({"Sound": data[0].strip("\n"), "Music": data[1].strip("\n")})


    def display_menu(self):
        self.run_display = True
        while self.run_display:
            with open("Txt_files\\volume.txt", "r") as file:
                data = file.readlines()
                self.Game.check_events()
                self.check_inputs()
                self.Game.display.fill(BLACK)
                self.Game.draw_text("Volume settings", 60, WIDTH / 2, 200)
                self.Game.draw_text("Sound:", 30, self.soundX, self.soundY)
                self.Game.draw_text(data[0].strip("\n"), 30, self.soundX + 90, self.soundY)
                self.Game.draw_text("Music:", 30, self.musicX, self.musicY)
                self.Game.draw_text(data[1].strip("\n"), 30, self.musicX + 90, self.musicY)
                self.draw_cursor()
                self.blit_screen()

    def check_inputs(self):
        self.move_cursor()
        if self.Game.START_KEY:
            with open("Txt_files\\volume.txt", "r+") as file:
                data = file.readlines()
                if self.cursor_state == "Sound":
                    self.increment_value(data[0], "Sound")
                    self.write_volume()

                elif self.cursor_state == "Music":
                    self.increment_value(data[1], "Music")
                    self.write_volume()

        if self.Game.BACK_KEY:
            self.Game.current_menu = self.Game.options_menu
            self.run_display = False

    def move_cursor(self):
        if self.Game.DOWN_KEY or self.Game.UP_KEY:
            if self.cursor_state == "Sound":
                self.cursor_rect.midtop = (self.musicX + self.offset, self.musicY)
                self.cursor_state = "Music"
            elif self.cursor_state == "Music":
                self.cursor_rect.midtop = (self.soundX + self.offset, self.soundY)
                self.cursor_state = "Sound"

    def increment_value(self, line, word):
        for key, value in self.volume_dict.items():
            if int(line) == int(value) and word == key:
                value = int(line)
                value += 1
                if value == 101:
                    value = 1
                self.volume_dict.update({key: value})

    def write_volume(self):
        with open("Txt_files\\volume.txt", "w") as file:
            volume_list = list(self.volume_dict.values())
            file.seek(0)
            file.write(str(volume_list[0]) + "\n")
            file.write(str(volume_list[1]) + "\n")
            file.truncate()


class ControlsMenu(Menu):
    def __init__(self, Game):
        Menu.__init__(self, Game)
        self.controls_dict = {}
        self.cursor_state = "Forward"
        self.options_list = ("Forward", "Back", "Left", "Right", "Use", "Inventory")
        with open("Txt_files\\controls.txt", "r") as file:
            data = file.readlines()
            for i in range(len(self.options_list)):
                self.controls_dict.update({self.options_list[i]: data[i].strip("\n")})
        self.cursor_rect.midtop = (self.menu_width - 50 + self.offset, self.menu_height - 40)
        self.editing = False
        self.current_operation = "Awaiting input"

    def display_menu(self):
        self.run_display = True
        while self.run_display:
            self.Game.check_events()
            self.check_inputs()
            self.Game.display.fill(BLACK)
            self.Game.draw_text('Controls', 60, WIDTH / 2, 200)
            self.draw_controls()
            self.draw_cursor()
            self.Game.draw_text(self.current_operation, 30, WIDTH / 2, 250)
            if self.Game.START_KEY:
                if self.editing is False:
                    self.editing = True
                else:
                    self.editing = False
                    self.current_operation = "Awaiting input"
            if self.editing:
                for key, value in self.controls_dict.items():
                    if self.cursor_state == key:
                        try:
                            self.current_operation = "Changing keybind: " + key
                            self.change_keybind(value)
                        except TypeError:
                            self.current_operation = "Key already taken, awaiting new input"
                            self.editing = False

            self.blit_screen()

    def draw_controls(self):
        yPos = self.menu_height - 40
        for key, value in self.controls_dict.items():
            self.Game.draw_text(key + ": " + value, 30, self.menu_width - 50, yPos)
            yPos += 70

    def check_inputs(self):
        self.move_cursor()
        if self.Game.BACK_KEY:
            self.Game.current_menu = self.Game.options_menu
            self.current_operation = "Awaiting input"
            self.run_display = False
            self.editing = False

    def change_keybind(self, value):
        key = pygame.key.get_pressed()
        for i in range(len(key))[32:]:
            if key[i]:
                with open("Txt_files\\controls.txt", "w") as file:
                    new_text = list(self.Game.game_controls.values())
                    file.seek(0)
                    string = chr(i)
                    if string in self.Game.game_controls.values():
                        for i in range(len(new_text)):
                            file.write(new_text[i] + "\n")
                        raise TypeError
                    for i in range(len(new_text)):
                        if new_text[i] == value:
                            file.write(str(string) + "\n")
                        else:
                            file.write(new_text[i] + "\n")
                    file.truncate()
                    with open("Txt_files\\controls.txt", "r") as file:
                        data = file.readlines()
                        for i in range(len(self.options_list)):
                            self.controls_dict.update({self.options_list[i]: data[i].strip("\n")})
                        for i in range(len(self.Game.key_list)):
                            self.Game.game_controls.update({self.Game.key_list[i]: data[i].strip("\n")})
                self.current_operation = "Awaiting input"
                self.editing = False
                break

    def move_cursor(self):
        if self.Game.DOWN_KEY:
            self.switcher(self.cursor_state, 'Down')
        elif self.Game.UP_KEY:
            self.switcher(self.cursor_state, 'Up')

    def switcher(self, state, direction):
        if direction == 'Down':
            cursor_states = {
                'Forward': ['Back', self.menu_width - 50, self.menu_height + 30],
                'Back': ['Left', self.menu_width - 50, self.menu_height + 100],
                'Left': ['Right', self.menu_width - 50, self.menu_height + 170],
                'Right': ['Use', self.menu_width - 50, self.menu_height + 240],
                'Use': ['Inventory', self.menu_width - 50, self.menu_height + 310],
                'Inventory': ['Forward', self.menu_width - 50, self.menu_height - 40],
            }
        else:
            cursor_states = {
                'Forward': ['Inventory', self.menu_width - 50, self.menu_height + 310],
                'Back': ['Forward', self.menu_width - 50, self.menu_height - 40],
                'Left': ['Back', self.menu_width - 50, self.menu_height + 30],
                'Right': ['Left', self.menu_width - 50, self.menu_height + 100],
                'Use': ['Right', self.menu_width - 50, self.menu_height + 170],
                'Inventory': ['Use', self.menu_width - 50, self.menu_height + 240],
            }
        parameters = cursor_states.get(state)
        self.move_function(parameters[0], parameters[1], parameters[2])

    def move_function(self, state, x, y):
        self.cursor_rect.midtop = (x + self.offset, y)
        self.cursor_state = state