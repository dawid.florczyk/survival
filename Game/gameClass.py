from Game.menuClasses import *
from Game.playerClasses import *
from Game.terrainClasses import *
from Game.settings import *
from Game.npcClasses import *
import p
ygame

class Game:
    def __init__(self):
        pygame.init()
        self.running = True
        self.playing = False
        self.UP_KEY, self.DOWN_KEY, self.START_KEY, self.BACK_KEY = False, False, False, False
        self.display = pygame.Surface((WIDTH, HEIGHT))
        self.window = pygame.display.set_mode((WIDTH, HEIGHT))
        self.font_name = pygame.font.get_default_font()
        self.main_menu = MainMenu(self)
        self.options_menu = OptionsMenu(self)
        self.volume_menu = VolumeMenu(self)
        self.controls_menu = ControlsMenu(self)
        self.current_menu = self.main_menu
        pygame.display.set_caption("Gra Survival")
        self.game_controls = {}
        self.key_list = (pygame.K_w, pygame.K_s, pygame.K_a, pygame.K_d, pygame.K_e, pygame.K_q)
        with open("Txt_files\\controls.txt", "r") as file:
            data = file.readlines()
            for i in range(len(self.key_list)):
                self.game_controls.update({self.key_list[i]: data[i].strip("\n")})
        self.all_sprites = pygame.sprite.Group()
        self.player = Player(self.game_controls, self.display)
        self.walls = pygame.sprite.Group()
        self.cow = Cow(self.display)
        self.Generator = Generating().Gener()

    def new(self):
        PositionY = 0
        for layer in self.Generator:
            PositionX = 0
            for tile in layer:
                if (tile == 0):
                    Grass(self,PositionX,PositionY)
                if (tile == 1):
                    Rock(self,PositionX,PositionY)
                if (tile == 2):
                    Tree(self,PositionX,PositionY)
                if (tile == 3):
                    Iron(self, PositionX, PositionY)
                if (tile == 4):
                    Gold(self, PositionX, PositionY)
                if (tile == 5):
                    AppleTree(self, PositionX, PositionY)
                PositionX += 1
            PositionY += 1

    def update(self):
        self.all_sprites.update()

    def search(self,):
        pos = ((0, -1), (0, 1), (-1, 0), (1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1))
        for i in pos:
            if self.player.collecting == True:
                if self.player.current_tool == "pickaxe":
                    if self.Generator[self.player.GameAxisY // TILESIZE + i[1] + 1][self.player.GameAxisX // TILESIZE + i[0] + 1] == 1:
                        Rock.interaction(self, self.player)
                    elif self.Generator[self.player.GameAxisY // TILESIZE + i[1] + 1][self.player.GameAxisX // TILESIZE + i[0] + 1] == 3:
                        Iron.interaction(self, self.player)
                    elif self.Generator[self.player.GameAxisY // TILESIZE + i[1] + 1][self.player.GameAxisX // TILESIZE + i[0] + 1] == 4:
                            Gold.interaction(self, self.player)
                elif self.player.current_tool == "axe":
                    if self.Generator[self.player.GameAxisY // TILESIZE + i[1] + 1][self.player.GameAxisX // TILESIZE + i[0] + 1] == 2:
                        Tree.interaction(self, self.player)
                elif self.player.current_tool == "hand":
                    if self.Generator[self.player.GameAxisY // TILESIZE + i[1] + 1][self.player.GameAxisX // TILESIZE + i[0] + 1] == 5:
                        AppleTree.interaction(self, self.player)
                #elif self.player.current_tool == "knife":
                    #TUTAJ TRZEBA DODAC ZABIJANIE KROWY
        self.player.collecting = False

    def print_resources(self):
        x = 50
        for key, value in self.player.materials.items():
            self.draw_text(key + ":" + str(value), 20, x, 750)
            x += 100

    def draw(self):
        self.display.fill(DARKGREY)
        self.all_sprites.draw(self.display)
        pygame.display.flip()
        self.display.blit(self.player.img, (self.player.GameAxisX, self.player.GameAxisY))
        self.print_resources()

    def print_death_screen(self):
        if self.player.current_hp == 0:
            self.display.fill(BLACK)
            self.draw_text("YOU DIED", 60, WIDTH / 2, HEIGHT / 2)

    def game_loop(self):
        self.new()
        while self.playing:
            pygame.time.Clock().tick(FPS)
            self.check_events()
            if self.START_KEY:
                self.playing = False
            self.reset_keys()
            self.update()
            self.draw()
            self.player.update()
            self.search()
            self.print_death_screen()
            self.display.blit(self.cow.img, (self.cow.AxisX_change, self.cow.AxisY_change))
            self.cow.idle()
            self.window.blit(self.display, (0, 0))

    def check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running, self.playing = False, False
                self.current_menu.run_display = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    self.START_KEY = True
                if event.key == pygame.K_BACKSPACE:
                    self.BACK_KEY = True
                if event.key == pygame.K_DOWN:
                    self.DOWN_KEY = True
                if event.key == pygame.K_UP:
                    self.UP_KEY = True

    def reset_keys(self):
        self.UP_KEY, self.DOWN_KEY, self.START_KEY, self.BACK_KEY = False, False, False, False

    def draw_text(self, text, size, x, y):
        font = pygame.font.Font(self.font_name, size)
        text_surface = font.render(text, True, WHITE)
        text_rect = text_surface.get_rect()
        text_rect.center = (x, y)
        self.display.blit(text_surface, text_rect)


