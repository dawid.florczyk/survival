import pygame
from Game.settings import RED, BROWN, TILESIZE, HEIGHT, WIDTH


class Key_handler:

    def __init__(self,key_):
        self.pressed = False
        self.Key = key_

    def getMouseClick(self):
        mouse_click = pygame.key.get_pressed()[self.Key]
        if (mouse_click == True):
            if (self.pressed == True):
                return False
            if (self.pressed == False):
                self.pressed = True
                return True
        else:
            self.pressed = False
        return False


class GameParticipant:
    def __init__(self, AxisX, AxisY, imgPath, hp):
        self.GameAxisX = AxisX
        self.GameAxisY = AxisY
        self.img = pygame.transform.scale(pygame.image.load(imgPath), (self.GameAxisX, self.GameAxisY))
        self.img_rect = self.img.get_rect()
        self.HP = hp
        self.current_hp = self.HP
        self.AxisX_change = self.AxisY_change = 0

    def gain_value(self, current, maximum, amount):
        if current < maximum:
            current += amount
        if current >= maximum:
             current = maximum
        return current

    def lose_value(self, current, amount):
        if current > 0:
            current -= amount
        if current < 0:
            current = 0
        return current


class Player(GameParticipant):
    def __init__(self, game_controls, display):
        GameParticipant.__init__(self, 50,50, "Images\\img.png.png", 100)
        self.img_rect.width -= 20
        self.img_rect.height -= 20
        self.game_controls = game_controls
        self.display = display
        self.materials = {'Wood': 0, 'Stone': 0, 'Gold' : 0, 'Iron': 0}
        self.healtbar_lenght = 400
        self.healthbar_ratio = self.HP / self.healtbar_lenght
        self.hunger_maximum = 100
        self.hunger_current = self.hunger_maximum
        self.hunger_icon = pygame.transform.scale(pygame.image.load("Images//chicken_wing.png"), (40, 40))
        self.knife_icon = pygame.transform.scale(pygame.image.load("Images//french-knife.png"), (80, 40))
        self.axe_icon = pygame.transform.scale(pygame.image.load("Images//axe.png"), (80, 40))
        self.hand_icon = pygame.transform.scale(pygame.image.load("Images/hand.png"), (80, 40))
        self.pickaxe_icon = pygame.transform.scale(pygame.image.load("Images//pickaxe.png"), (80, 40))
        self.tools_tuple = ("pickaxe", "axe", "knife", "hand")
        self.tools_icon_tuple = (self.pickaxe_icon, self.axe_icon, self.knife_icon, self.hand_icon)
        self.current_tool_icon = self.tools_icon_tuple[0]
        self.current_tool = "pickaxe"
        self.collecting = False

        self.keyHandlerList = []
        for i in self.game_controls:
            self.keyHandlerList.append(Key_handler(ord(self.game_controls[i])))

    def change_tool(self):
            for i in range(len(self.tools_tuple)):
                if self.current_tool == self.tools_tuple[i]:
                    if self.tools_tuple[i] != "hand":
                        self.current_tool = self.tools_tuple[i + 1]
                        self.current_tool_icon = self.tools_icon_tuple[i + 1]
                        break
                    else:
                        self.current_tool = self.tools_tuple[0]
                        self.current_tool_icon = self.tools_icon_tuple[0]
                        break
            return self.current_tool, self.current_tool_icon

    def draw_healhbar(self):
        pygame.draw.rect(self.display, (RED), (self.GameAxisX - 20, self.GameAxisY - 20, self.current_hp, 15))

    def draw_hunger(self):
        pygame.draw.rect(self.display, (BROWN), (920, 730, self.hunger_current, 20))
        self.display.blit(self.hunger_icon, (870, 720))
        if self.hunger_current == 0:
            self.current_hp = self.lose_value(self.current_hp, 0.35)

    def update(self):
        self.movement()
        self.draw_healhbar()
        self.draw_hunger()
        self.display.blit(self.current_tool_icon, (760, 720))

    def movement(self):
        key_pressed = pygame.key.get_pressed()
        for i in self.keyHandlerList:
            if (key_pressed[i.Key] == 0):
                i.pressed = False

        for i in range(len(key_pressed))[32:]:
            if key_pressed[i]:
                key_value = chr(i)
                for key, value in self.game_controls.items():
                    if key_value == value:
                        for handler in self.keyHandlerList:
                            if (handler.getMouseClick() == True):
                                if key == pygame.K_d:
                                    self.AxisX_change += TILESIZE
                                    self.hunger_current = self.lose_value(self.hunger_current, 0.30)
                                elif key == pygame.K_w:
                                    self.AxisY_change -= TILESIZE
                                    self.hunger_current = self.lose_value(self.hunger_current, 0.30)
                                elif key == pygame.K_s:
                                    self.AxisY_change += TILESIZE
                                    self.hunger_current = self.lose_value(self.hunger_current, 0.30)
                                elif key == pygame.K_a:
                                    self.AxisX_change -= TILESIZE
                                    self.hunger_current = self.lose_value(self.hunger_current, 0.30)
                                elif key == pygame.K_e:
                                    self.collecting = True
                                elif key == pygame.K_q:
                                    self.current_tool, self.current_tool_icon = self.change_tool()

        self.GameAxisX += self.AxisX_change
        self.GameAxisY += self.AxisY_change

        if self.GameAxisX <= 0:
            self.GameAxisX = 0
        elif self.GameAxisX >= WIDTH - 32:
            self.GameAxisX = WIDTH - 32

        if self.GameAxisY >= HEIGHT - 140:
            self.GameAxisY = HEIGHT - 140
        elif self.GameAxisY <= 0:
            self.GameAxisY = 0

        if self.AxisX_change != 0:
            self.AxisX_change = 0
        if self.AxisY_change != 0:
            self.AxisY_change = 0
